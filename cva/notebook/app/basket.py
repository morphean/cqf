from accelerate import cuda
from itertools import cycle

import matplotlib
import matplotlib.pyplot as plt
from numpy import linspace, array, arange
from pandas import DataFrame, ExcelWriter
from scipy.interpolate import UnivariateSpline
from scipy.stats import t

import BasketCDS as bds
import Copula
import rng

cycol = cycle('cmy').next

gpuEnabled = cuda.cuda_compatible()

print 'GPUGP libraries detected, enabling GPU for rng' if gpuEnabled else 'not using GPU'

# set defaults for charts
matplotlib.rcParams['figure.figsize'] = (20.0, 10.0)
matplotlib.rcParams['font.size'] = 14.0

# plot yield curves
usYc = [0.555, 0.758, 0.862, 1.008302632, 1.135, 1.396]
autYc = [-0.515, -0.518, -0.49, -0.469, -0.384, -0.35]
beYc = [-0.602, -0.45, -0.551, -0.425, -0.412, -0.381]
frYc = [-0.545, -0.54, -0.497, -0.458, -0.347, -0.297]
deYc = [-0.616, -0.599, -0.625, -0.598, -0.484, -0.473]
itaYc = [-0.205, -0.019, 0.018, 0.125, 0.321, 0.499]
x = arange(1, 7)
timesteps = x


# interpolate for smooth curve
def plotInterpolatedCurveWithOrig(xdata=array, ydata=array, label=str):
    lincol = cycol()
    plt.plot(xdata, ydata, 'ro', c=lincol, ms=5, label=label)
    spl = UnivariateSpline(xdata, ydata)
    xs = linspace(min(xdata), len(xdata), 1000)
    spl.set_smoothing_factor(0.5)
    plt.plot(xs, spl(xs), ':k', c=lincol, lw=2)


def plotYieldCurve():
    plt.grid()
    plt.title('Yield Curves for Basket against US Treasury\n')
    plt.xlabel('Maturity')
    plt.ylabel('Yield (%)')
    plt.xlim(0, 7)
    plotInterpolatedCurveWithOrig(x, beYc, 'Belgium 5Y')
    plotInterpolatedCurveWithOrig(x, autYc, 'Austria 5Y')
    plotInterpolatedCurveWithOrig(x, frYc, 'France 5Y')
    plotInterpolatedCurveWithOrig(x, deYc, 'Germany 5Y')
    plotInterpolatedCurveWithOrig(x, itaYc, 'Italy 5Y')
    plotInterpolatedCurveWithOrig(x, usYc, 'US 5Y')
    plt.legend(loc='upper left')
    plt.show()


def generateRandomSampleMatrix(sims=int, basketSize=1):
    """
    generate a matrix of two independent standard normal variables,
    $Z_1, Z_2$

    nb: it will source these via GPU is CUDA drivers are installed and configured correctly.
    :param sims:
    :return:
    """
    shape = (basketSize, sims)
    if gpuEnabled:
        return rng.getPseudoRandomNumbers_Standard_cuda(shape)

    return rng.getPseudoRandomNumbers_Standard(shape)


def generateRandomTSampleMatrix(sims=int, basketSize=1, dof=2.74335149908):
    """
    generate a matrix of n independent variables from t distribution,
    $Z_1, Z_2$

    nb: not available on GPU
    :param sims:
    :return:
    """

    return t.rvs(df=dof, size=(sims, basketSize))


# plotYieldCurve()

# Generate Term structure from hazard rates
timesteps = range(0, 6)
baseRate = 0.0039
autDF = bds.BasketCDS.genDiscountFactors(timesteps, baseRate)

autData = [{'year': 1, 'spread': 115.70, 'Psurv': 0.982479122318651, 'hzdRate': 0.00767666961222201},
           {'year': 2, 'spread': 120.80, 'Psurv': 0.958704317759161, 'hzdRate': 0.01063864706076580},
           {'year': 3, 'spread': 141.30, 'Psurv': 0.935158751440165, 'hzdRate': 0.01079934088002760},
           {'year': 4, 'spread': 142.35, 'Psurv': 0.907384926248782, 'hzdRate': 0.01309378208757440},
           {'year': 5, 'spread': 154.70, 'Psurv': 0.881781194909988, 'hzdRate': 0.01243072764491560}]
belData = [{'year': 1, 'spread': 115.70, 'Psurv': 0.981081478816816, 'hzdRate': 0.00829492296551629},
           {'year': 2, 'spread': 120.80, 'Psurv': 0.960898216825752, 'hzdRate': 0.00902768958653353},
           {'year': 3, 'spread': 141.30, 'Psurv': 0.932269105082887, 'hzdRate': 0.01313609528367980},
           {'year': 4, 'spread': 142.35, 'Psurv': 0.910168391619283, 'hzdRate': 0.01041954293598230},
           {'year': 5, 'spread': 154.70, 'Psurv': 0.879621843358195, 'hzdRate': 0.01482574371445270}]
fraData = [{'year': 1, 'spread': 110.00, 'Psurv': 0.981996726677578, 'hzdRate': 0.00788995985891059},
           {'year': 2, 'spread': 129.80, 'Psurv': 0.958018344322737, 'hzdRate': 0.01073621502719710},
           {'year': 3, 'spread': 135.90, 'Psurv': 0.934859797608484, 'hzdRate': 0.01062734118316680},
           {'year': 4, 'spread': 145.65, 'Psurv': 0.908106923903281, 'hzdRate': 0.01260949694348420},
           {'year': 5, 'spread': 148.20, 'Psurv': 0.884632754882147, 'hzdRate': 0.01137397123783840}]
gerData = [{'year': 1, 'spread': 117.10, 'Psurv': 0.980856942015007, 'hzdRate': 0.00839432985148177},
           {'year': 2, 'spread': 135.70, 'Psurv': 0.956178825998711, 'hzdRate': 0.01106654787067210},
           {'year': 3, 'spread': 148.70, 'Psurv': 0.928935070875451, 'hzdRate': 0.01255376280085040},
           {'year': 4, 'spread': 159.65, 'Psurv': 0.899723183928248, 'hzdRate': 0.01387644802038900},
           {'year': 5, 'spread': 161.90, 'Psurv': 0.874694817786536, 'hzdRate': 0.01225235794298400}]
itaData = [{'year': 1, 'spread': 76, 'Psurv': 0.987491770901909, 'hzdRate': 0.00546651480710512},
           {'year': 2, 'spread': 77.7, 'Psurv': 0.974589948425733, 'hzdRate': 0.00571155728341799},
           {'year': 3, 'spread': 84.4, 'Psurv': 0.958897577274220, 'hzdRate': 0.00704970655753197},
           {'year': 4, 'spread': 87.35, 'Psurv': 0.943711257709229, 'hzdRate': 0.00693308549188108},
           {'year': 5, 'spread': 81.4, 'Psurv': 0.934898164118376, 'hzdRate': 0.00407482891279857}]

hazardDataA = DataFrame(autData)
hazardDataB = DataFrame(belData)
hazardDataF = DataFrame(fraData)
hazardDataG = DataFrame(gerData)
hazardDataI = DataFrame(itaData)

autCDS = bds.BasketCDS(N=1, timesteps=timesteps, discountFactors=autDF, lamda=hazardDataA['hzdRate'], Dt=1)

# gaussian copula sampling

gpuEnabled = False
numberOfSimulations = 10
z = generateRandomSampleMatrix(numberOfSimulations)
tSim = generateRandomTSampleMatrix(numberOfSimulations)

RHO = 0.01
# simulating Copula functions
# g = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', intensity=(reference.intensity, reference2.intensity))
t = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='t', lamda=hazardDataA['hzdRate'])
t1 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='t', lamda=hazardDataB['hzdRate'])
t2 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='t', lamda=hazardDataF['hzdRate'])
t3 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='t', lamda=hazardDataG['hzdRate'])
t4 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='t', lamda=hazardDataI['hzdRate'])

g = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', lamda=hazardDataA['hzdRate'])
g1 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', lamda=hazardDataB['hzdRate'])
g2 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', lamda=hazardDataF['hzdRate'])
g3 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', lamda=hazardDataG['hzdRate'])
g4 = Copula.simulateCopula(numberOfSimulations, rho=RHO, type='g', lamda=hazardDataI['hzdRate'])

tc = DataFrame()
tc['u1'] = t['u1']
tc['u2'] = t['u2']
tc['u3'] = t['u3']
tc['u4'] = t['u4']
tc['u5'] = t['u5']
tc.corr()

gc = DataFrame()
gc['u1'] = g['u1']
gc['u2'] = g['u2']
gc['u3'] = g['u3']
gc['u4'] = g['u4']
gc['u5'] = g['u5']
gc.corr()
tc1 = tc.corr(method='spearman')
tc2 = tc.corr(method='kendall')
# G = cholesky(gc.corr(), lower=True)

print 'pearson \n', tc
print 'spearman \n', tc1
print 'kendall \n', tc2

from sklearn import preprocessing

x = tc2.values  # returns a numpy array
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
df = DataFrame(x_scaled)


def outputToExcel():
    # output the files to csv for import into Excel

    writer = ExcelWriter('copulaSimulation-web.xlsx')
    t.to_excel(writer, 'T-Austria')
    t1.to_excel(writer, 'T-Belgium')
    t2.to_excel(writer, 'T-France')
    t3.to_excel(writer, 'T-Germany')
    t4.to_excel(writer, 'T-Italy')

    g.to_excel(writer, 'G-Austria')
    g1.to_excel(writer, 'G-Belgium')
    g2.to_excel(writer, 'G-France')
    g3.to_excel(writer, 'G-Germany')
    g4.to_excel(writer, 'G-Italy')

    writer.save()

    print 'copula simulation data written to \'./copulationSimulation2.xlsx\''

print True
