import time

from numpy import linspace
from scipy.optimize import fsolve

import CreditDefaultSwap as cds
import rng
from LMM1F import LMM1F

gpuEnabled = False


# pushing the size of rng generated above 100000 causes GPU to run out of space
# possible optimisation is to load it into a 3d vector shape instead of flat structure.

def initRandSource():
    randN = 100000
    randSourceGPU = rng.getPseudoRandomNumbers_Uniform_cuda(randN) if gpuEnabled else []
    randSourceCPU = rng.getPseudoRandomNumbers_Uniform(randN)
    return randSourceGPU if gpuEnabled else randSourceCPU


randSource = initRandSource()

debug = False

# 5Y tenor
noOfYears = 5.
# 6M payments
paymentFrequency = 0.5
yearFraction = paymentFrequency / noOfYears
noOfPayments = noOfYears / paymentFrequency

# no of timesteps
timesteps = linspace(0, noOfYears, noOfPayments + 1)

# test vars
# testRates = array([0.01, 0.03, 0.05, 0.07])
# df = [0.0, 0.9975, 0.9900, 0.9778, 0.9608]
# seed = 0.01
# payments = 4

initRates_BOE_6m = [0.423546874, 0.425980925, 0.45950348, 0.501875772, 0.551473011, 0.585741857,
                    0.626315731,
                    0.667316554, 0.709477279, 0.753122018]

initRates_BOE_6m_scaled = [x * 1 / 100. for x in initRates_BOE_6m]
BOE_6m_discountFactors = cds.genDiscountFactors(timesteps, initRates_BOE_6m_scaled)
BOE_6m_fwdCurve = cds.genFwdCurve(timesteps, initRates_BOE_6m_scaled)

notional = 1000000

# fixed hazard rates
lamda = 0.03
seed = 0.01

# before calibration

f = cds.CreditDefaultSwap(N=notional,
                          timesteps=timesteps,
                          discountFactors=BOE_6m_discountFactors,
                          lamda=lamda)
print 'MTM before calibration ', f.markToMarket, '\n'


# we use seed as our initial guess for the optimisation
def calibrateCDS(lamda=float, seed=0.01):
    # calibration method
    e = cds.CreditDefaultSwap(N=notional,
                              timesteps=timesteps,
                              discountFactors=BOE_6m_discountFactors,
                              lamda=lamda,
                              spread=seed)
    return e.markToMarket


optimisation = fsolve(calibrateCDS, x0=[lamda], full_output=True)

print '================'
print 'Calibration Results using seed of ', seed
print 'value:\t\t\t', optimisation[0]
print 'iterations:\t', optimisation[1]['nfev']
print optimisation[3], '\n'

calibratedLambda = optimisation[0]
# then set seed to be the same value as the calibrated lambda value
f = cds.CreditDefaultSwap(N=notional,
                          timesteps=timesteps,
                          discountFactors=BOE_6m_discountFactors,
                          lamda=calibratedLambda)

print 'MTM After calibration: ', f.markToMarket, '\n'

start_time = time.clock()

# simulate L6M
irEx = LMM1F(nSims=100,
             initialSpotRates=initRates_BOE_6m_scaled,
             dT=paymentFrequency,
             notional=notional,
             randSource=randSource)
irExData = irEx.getSimulationData()

exposure = f.getExpectedExposureA(irExData)

exposureB = f.getExpectedExposureB(irExData)

exposure97 = f.getPercentile(irExData, 97.5)

mtm = f.getExpectedMTM(irExData)

# exposure = array([462.5088523,1344.262261,1254.430152,1095.070118])
cvaA = f.calcCVA(mtm, exposure)
cvaB = f.calcCVA(mtm, exposureB)
cva97 = f.calcCVA(mtm, exposure97)
# bva = cva - c.calcCVA(c.getExpectedExposureB(a))

print '====='
print 'cva A: ', cvaA
print 'cva B: ', cvaB
print 'cva using PFE (97.5): ', cva97
print '====='

# print 'bva: ', bva

# from
# initRates = [0.004625384831,0.006812859244,0.009606329010,0.012040577151,0.013898638373,0.015251633803,0.016212775988,0.016899521932,0.017420140777,0.017841154790]
initRates_BOE_SPT = [0.461370334, 0.452898877, 0.443905273, 0.435584918, 0.428713527, 0.423546874, 0.420158416,
                     0.418516458, 0.41849405, 0.419889508, 0.42246272, 0.425980925, 0.430265513, 0.43519681,
                     0.440673564, 0.44660498, 0.452907646, 0.45950348, 0.466318901, 0.473289989, 0.480366388,
                     0.487508789, 0.494686524, 0.501875772, 0.509058198, 0.516220029, 0.523351338, 0.530445369,
                     0.537497985, 0.544507238, 0.551473011, 0.558396735, 0.565281153, 0.572130127, 0.578948479,
                     0.585741857, 0.592516573, 0.599278976, 0.606034922, 0.612789787, 0.619548528, 0.626315731,
                     0.633095655, 0.63989227, 0.646709286, 0.653550185, 0.660418244, 0.667316554, 0.67424803,
                     0.681215261, 0.688220353, 0.695264957, 0.702350311, 0.709477279, 0.716646384, 0.723857839,
                     0.731111575, 0.738407263, 0.745744337, 0.753122018]

