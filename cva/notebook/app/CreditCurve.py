from math import isnan

from numpy import array, ones, zeros, exp, put, sum, mean, percentile, append, linspace
from pandas import DataFrame

from utils import getTPlusFromList

debug = False


class CreditCurve(object):
    """
    enter spread value in normal number and NOT in basis points
    TODO - allow input of basis points for spread
    """

    def __init__(self,
                 N=float,
                 timesteps=list,
                 discountFactors=list,
                 lamda=float,
                 spread=float('nan'),
                 Dt=0.5,
                 recoveryRate=0.4,
                 llamda=list):
        self.N = N  # notional
        self.__lamda = lamda
        self.__seed = spread
        self.recoveryRate = recoveryRate
        self.Dt = Dt
        self.__timesteps = timesteps
        self.discountFactors = discountFactors

        # if llamda is list and len(llamda) > 0:
        #     self.pT = self.generateProbSurvivalTS()
        #     self.premiumLegsSpread = self.calculatePremiumLegSpreadsTS()
        # else:
        self.pT = self.generateProbSurvival()
        self.premiumLegsSpread = self.calculatePremiumLegValues()

        self.__premiumLegSum = sum(self.premiumLegsSpread)
        self.pD = self.generateProbDefault()
        self.defaultLegsSpread = self.calculateDefaultLegValues()
        self.__defaultLegSum = sum(self.defaultLegsSpread)
        self.fairSpread = self.premiumLegsSpread / self.defaultLegsSpread
        self.__expectedExposure = []

    @property
    def premiumLegSum(self):
        return self.__premiumLegSum

    @property
    def defaultLegSum(self):
        return self.__defaultLegSum

    @property
    def markToMarket(self):
        return self.__premiumLegSum - self.__defaultLegSum

    @property
    def lamda(self):
        return self.__lamda

    @property
    def seed(self):
        return self.__seed

    @property
    def timesteps(self):
        return self.__timesteps

    def generateDiscountFactors(self):
        pass

    def generateProbSurvival(self):
        """
        using $\exp^{-\lambda*T_i}$
        :return:
        """
        pt = ones(len(self.timesteps))
        for index, t in enumerate(self.timesteps):
            ps = exp(self.lamda * -1 * t) if index > 0 else 1.0
            put(pt, index, ps)
        return pt

    def generateProbDefault(self):
        """
        using $P(T,0) = P_{i-1} - P_i$
        :return:
        """
        pd = zeros(len(self.timesteps))
        for index, pt in enumerate(self.pT):
            pdi = self.pT[index - 1] - pt if index > 0 else 0.0
            put(pd, index, pdi)

        return pd

    def calculatePremiumLegValues(self, withAccruals=False):
        """
        returns the list of the premium leg values
        :return: array
        """
        #          assume 1%
        values = zeros(len(self.timesteps))

        for index, (df, pt, pd) in enumerate(zip(self.discountFactors, self.pT, self.pD)):
            if index > 0:
                pv = (self.N * self.Dt * df * pt) * (self.lamda if isnan(self.seed) else self.seed)
                if withAccruals:
                    # $ +D(0,T_n)(P(T_{n-1}) - P(T_n)) \frac{\Delta t_n}{2} $
                    pv += pd * self.Dt * 0.5
                put(values, index, pv)
        return values

    def calculateDefaultLegValues(self):
        """
        returns the list of the default leg values
        :return: array
        """
        #          assume 1%
        spreads = zeros(len(self.timesteps))
        for index, (df, pd) in enumerate(zip(self.discountFactors, self.pD)):
            if index > 0:
                spread = self.N * (1 - self.recoveryRate) * df * pd
                put(spreads, index, spread)
        return spreads

    def calcCVA(self, mtm=array, expectedExposure=array):
        cvaData = DataFrame()
        cvaData['t'] = self.timesteps
        cvaData['discountFactor'] = self.discountFactors
        cvaData['pSurvival'] = self.pT
        cvaData['pDefault'] = self.pD
        cvaData['mtm'] = mtm
        cvaData['1-R'] = [1 - self.recoveryRate] * len(self.pD)
        cvaData['exposure'] = [getTPlusFromList(expectedExposure, i, True) for i in range(len(expectedExposure))]
        cvaData['cvaPerTimeStep'] = cvaData['discountFactor'] * cvaData['pDefault'] * cvaData['1-R'] * cvaData[
            'exposure']
        cva = cvaData['cvaPerTimeStep'].sum()
        print cvaData
        # print 'CVA = ', cva
        return cva

    def calcBVA(self, eeA=array, eeB=array):
        # LAST BIT TO DO
        return True

    def getExpectedExposureA(self, simData=array):
        expectedExposure = mean(array([sim.expA for sim in simData]), axis=0)
        return append(expectedExposure, 0.0)

    def getExpectedExposureB(self, simData=array):
        expectedExposure = mean(array([sim.expB for sim in simData]), axis=0)
        return append(expectedExposure, 0.0)

    def getPercentile(self, simData=array, p=float):
        ptile = percentile(array([sim.mtm for sim in simData]), q=p, axis=0)
        return append(ptile, 0.0)

    def getExpectedMTM(self, simData=array):
        expected_mtm = mean(array([sim.mtm for sim in simData]), axis=0)
        return append(expected_mtm, 0.0)

        # @classmethod
        # def calculatePremiumLegSpreadsTS(self):
        #     """
        #     returns the list of the premium leg values using the supplied term structure for lamda (hazard rate)
        #     :return: array
        #     """
        #     #          assume 1%
        #     spreads = zeros(len(self.timesteps))
        #     for index, (df, pt, l) in enumerate(zip(self.discountFactors, self.pT, self.llamda)):
        #         if index > 0:
        #             spread = (self.N * self.Dt * df * pt) * l
        #             put(spreads, index, spread)
        #     return spreads

        # @classmethod
        # def generateProbSurvivalTS(self):
        #     """
        #     using $\exp^{-\lambda*T_i}$
        #     :return:
        #     """
        #     pt = ones(len(self.timesteps))
        #     for index, t, l in enumerate(zip(self.timesteps, self.llamda)):
        #         ps = exp(l * -1 * t) if index > 0 else 1.0
        #         put(pt, index, ps)
        #     return pt
        # def plotExpectedExposure(self, simData=array):
        #     # plot simulationsz
        #     cycol = cycle('cmy').next
        #
        #     for simulation in a:
        #         x, y = timesteps, append(simulation.mtm, 0)
        #         # print y[0]
        #         pyplot.plot(x, y, 's', c=cycol(), lw=0.5, alpha=0.6)
        #
        #     # get expected Exposure
        #     expectedExposure = self.getExpectedExposureA(simData=simData)
        #     ninetySevenP5 = self.getPercentile(simData=simData, p=97.5)
        #     twoP5 = self.getPercentile(simData=simData, p=2.5)
        #
        #     # rate from B91 on ukois16_mdaily.xls
        #     initRates_BOE_6m_plot = insert(initRates_BOE_6m, 0, 0.463126310164261)
        #     # add to plot
        #     # pyplot.plot(x, twoP5,c='#000000', lw=2, alpha=0.8)
        #     pyplot.plot(x, append(ninetySevenP5, 0.0), c='#00CC00', lw=2, alpha=0.8)
        #     pyplot.plot(x, append(twoP5, 0.0), c='#0000CC', lw=2, alpha=0.8)
        #     pyplot.plot(x, append(expectedExposure, 0.0), c='#FF0000', lw=2, alpha=0.8)
        #     # pyplot.plot(x, append(initRates_BOE_6m,0.0), lw=2, alpha=0.8)
        #     # pyplot.plot(x, initRates, lw=2, alpha=0.8)
        #     pyplot.xlabel('$\Delta t$')
        #     pyplot.xticks(timesteps)
        #     pyplot.title('Simulated $L_{6M}$')
        #     pyplot.ylabel('Payment')
        #     pyplot.legend()
        #     pyplot.grid()
        #     pyplot.show()


class Simulation(object):
    """
    this class is used a container for a single simulation
    providing convenenience methods to retrieve
    markToMarket values
    eeA = expected exposure from the simulation for Counterparty A
    eeB = expected exposure from the simulation for Counterparty B
    default fixed rate is set to current BoE base rate
    """

    def __init__(self, lbT=array, dfTable=array, notional=1000000, dt=0.25, k=0.004):
        """

        :type k: float
        :type dt: float
        :type notional: float
        :type dfTable: ndarray
        :type lbT: ndarray
        """
        self.liborTable = lbT

        self.dfTable = dfTable
        self.dt = dt

        # calculate payments for each timestep using the given notional, tenor, fixed rate,
        # floating(simulated) and discount factors (simulated)
        self.payments = self.calcPayments(notional, self.dt, k)

        self.mtm = array([flt - fxd for flt, fxd in self.payments])

        # exposure for counterParty A (using positive mtm)
        self.expA = array([max(L - K, 0) for L, K in self.payments])

        # exposure for counterParty B (using negative mtm)
        self.expB = array([min(L - K, 0) for L, K in self.payments])

    def calcPayments(self, notional=float, dt=float, fixed=-1.0):
        global debug
        """
        calculate payments for the simulation of the Fwd rates and discount factors
        given notional and tenor

        :param notional:
        :param dt:
        :param fixed:
        :return: float
        """
        payments = []

        for index in range(0, len(self.liborTable)):
            fwdCurve = self.liborTable[:, index]
            df = self.dfTable[1:, index]

            floatingLeg = [fwd * dfi * notional * dt for fwd, dfi in zip(fwdCurve, df)]
            fixedLeg = [fixed * dfi * notional * dt for dfi in df]
            # fixedLeg[len(self.__liborTable)] = 0
            payments.append([sum(floatingLeg), sum(fixedLeg)])

            if debug:
                print 'from t-', index, '--- fixed - ', sum(fixedLeg), '--- floating -', sum(
                    floatingLeg), '-- mtm --', sum(floatingLeg) - sum(fixedLeg)
                print fixedLeg
                print '--'
                print floatingLeg
                print '--'

        return payments


def genDiscountFactors(t=list, s=list):
    discount = [float('nan')]

    for (ti, si) in zip(t[1:], s):
        DFi = exp(si * ti * -1)
        discount.append(DFi)

    return discount


# plot tbis against reference curve
def genFwdCurve(t=list, s=list):
    curve = []

    for i, (ti, si) in enumerate(zip(t, s)):
        sim1 = s[i - 1] if i > 0 else 0.0
        tim1 = t[i - 1] if i > 0 else 0.0
        Li = ((si * ti) - (sim1 * tim1)) / (ti - tim1) if i > 0 else 0.0
        curve.append(Li)
    return curve


def bootstrapSurvivalProbability(recovery=float, spreads=list, timesteps=list, discountFactors=list):
    # P(t_0) = 1
    pS = [1.0]
    L = 1 - recovery
    for index, (spread, timestep, d) in enumerate(zip(spreads, timesteps, discountFactors)):
        # note the zero offset here for year indices vs array indicies
        if index == 0:
            result = L / (L + timestep * spread)
        if index > 1:
            iM1 = index - 1
            result = (
                     discountFactors[iM1] * (L - (L + timesteps[iM1] * spread) * pS[iM1]) / d * (L + timestep * spread)) \
                     + (pS[iM1] * L) / (L + timestep * spread)
        pS.append(result)
    return pS


def genDiscountFactorForContinousRate(r=float, t=float):
    return exp(-t * r)


if __name__ == "__main__":
    timesteps = linspace(0, 3, 12)

    discountFactors2 = [genDiscountFactorForContinousRate(0.008, year) for year in range(1, 6)]

    scaledSpreads = [a / 10000. for a in [100, 150, 200, 250, 300]]

    impliedSurvivalProbabilities = bootstrapSurvivalProbability(recovery=0.4,
                                                                spreads=scaledSpreads,
                                                                timesteps=[1, 2, 3, 4, 5],
                                                                discountFactors=discountFactors2)
    # df = [0.9803, 0.9514, 0.9159, 0.8756, 0.8328]
    # seed = 0.01
    # payments = 4
    # notional = 1000000
    # # fixed hazard rates
    # lamda = 0.03
    #
    # b = BasketCDS(N=notional, timesteps=5, discountFactors=df, lamda=lamda, dt=1.0, seed=seed,)
    # b.recoveryRate = 0.5
    # b.buildFlatTermStructure([57]*5)
